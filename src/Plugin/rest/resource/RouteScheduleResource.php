<?php

namespace Drupal\gtfs_endpoint_schedule\Plugin\rest\resource;

use PDO;
use Drupal;
use DateTime;
use Drupal\gtfs\Plugin\rest\resource\ResourceResponse;
use Drupal\gtfs\Plugin\rest\resource\GTFSResourceBase as parentAlias;
use Drupal\gtfs\Entity\CalendarDate;
use Drupal\gtfs\Entity\StopTime;
use Drupal\gtfs\Entity\Stop;
use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Service;
use Drupal\gtfs\Entity\Direction;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides GTFS route schedules as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_route_schedule_resource",
 *   label = @Translation("GTFS route schedule REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/schedules"
 *   }
 * )
 */
class RouteScheduleResource extends parentAlias {

  public static $url = '/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/schedules';

  /**
   *
   */
  public function sanitizeFilter($filter, $style = 'v1') {
    $sanitizers = [
      'stop_id' => $style === 'v1'
        ? function ($stop_id) {
          $stop_ids = explode(',', $stop_id);
          $stop_ids = implode(',', array_map(function ($stop_id) {
            return Drupal::database()->query(
              'SELECT `id`
               FROM {gtfs_stop_field_data}
               WHERE `stop_id` = :stop_id',
               [':stop_id' => $stop_id]
            )->fetchColumn();
          }, $stop_ids));
          return $stop_ids;
        }
        : function ($stop_id) {
          $stop_ids = explode(',', $stop_id);
          $stop_ids = implode(',', array_map(function ($stop_id) {
            return Drupal::database()->query(
              'SELECT `stop_id`
               FROM {gtfs_stop_source}
               WHERE `stop_id` = :stop_id',
              [':stop_id' => $stop_id]
            )->fetchColumn();
          }, $stop_ids));
          return $stop_ids;
        },
    ];

    foreach ($filter as $key => $constraints) {
      if (isset($sanitizers[$key])) {
        foreach ($constraints as $operator => $value) {
          $filter[$key][$operator] = $sanitizers[$key]($value);
        }
      }
    }
    return $filter;
  }

  /**
   *
   */
  public function initializeMeta(): array {
    $meta = parent::initializeMeta();

    $direction_id = $this->currentRequest->query->get('direction_id');

    if (NULL !== $direction_id) {
      $meta['direction_id'] = $direction_id;
    }

    $service_id = $this->currentRequest->query->get('service_id');

    if (NULL !== $service_id) {
      $meta['service_id'] = $service_id;
    }

    $filter = $this->currentRequest->query->get('filter');

    // We have a two-step process for filters. First we determine
    // if they're valid, and then in self::sanitizeFilter, we
    // transform the data, for example if stop_id really needs
    // to reference an entity->id. The result of this sanitization
    // we won't return to the client, because it references
    // Drupal's internal structure.
    $allowed_keys = [
      'stop_id', 'timepoint',
    ];

    $allowed_operators = [
      'IN', '=', 'NOT IN', '!=',
    ];

    if (NULL !== $filter && is_array($filter)) {
      $filter = array_filter($filter, function ($constraints, $key) use ($allowed_keys, $allowed_operators) {
        if (!in_array($key, $allowed_keys)) {
          return FALSE;
        }
        foreach ($constraints as $operator => $value) {
          if (!in_array($operator, $allowed_operators)) {
            return FALSE;
          }
        }
        return TRUE;
      }, ARRAY_FILTER_USE_BOTH);
      $meta['filter'] = $filter;
    }

    return $meta;
  }

  /**
   *
   */
  public function getServiceAndCalendarDate($agency, $route) {
    $service_id = $this->currentRequest->query->get('service_id');
    $calendar_date = NULL;
    if ($this->currentRequest->query->get('date')) {
      $date = DateTime::createFromFormat(CalendarDate::GTFS_FORMAT, $this->currentRequest->query->get('date'));
      if (!$date) {
        throw new BadRequestHttpException('🖕');
      }
      $calendarDates = Drupal::entityTypeManager()->getStorage('gtfs_calendar_date')->loadByProperties([
        'date' => $date->format(CalendarDate::DB_FORMAT),
      ]);
      $service_removed = FALSE;
      $service_added = FALSE;
      $route_service_ids = array_map(function ($service) {
        return $service->id();
      }, $route->services());
      foreach ($calendarDates as $calendarDate) {
        $exception_type = $calendarDate->get('exception_type')->value;
        if ($exception_type === CalendarDate::EXCEPTION_TYPE_ADDED && in_array($calendarDate->get('service_id')->target_id, $route_service_ids)) {
          $service_added = $calendarDate->get('service_id')->target_id;
          $calendar_date = $calendarDate;
        }
        elseif ($exception_type === CalendarDate::EXCEPTION_TYPE_REMOVED) {
          $service_removed = TRUE;
        }
      }
      if ($service_removed && !$service_added) {
        $service_id = FALSE;
      }
      elseif (!!$service_added) {
        $service_id = Service::load($service_added)->get('service_id')->value;
//        $service_id = $service_added;
      }
    }
    $service_id = Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_service_field_data}
       WHERE `service_id` = :service_id
       AND `feed_reference__target_id` = :frti",
       [
        ':service_id' => $service_id,
        ':frti' => $agency->get('feed_reference')->target_id,
       ]
    )->fetchColumn();
    $service = Service::load($service_id);
    return [$service, $calendar_date];
  }

  public function getServiceAndCalendarDateFromSource($agency_id, $route_id) {
    $service_id = $this->currentRequest->query->get('service_id');
    $db = \Drupal::database();
    $calendar_date = NULL;
    if ($this->currentRequest->query->get('date')) {
      $date = DateTime::createFromFormat(CalendarDate::GTFS_FORMAT, $this->currentRequest->query->get('date'));
      if (!$date) {
        throw new BadRequestHttpException('🖕');
      }
      $calendarDates = $db->query('
          SELECT *
          FROM {gtfs_calendar_date_source}
          WHERE `date` = :date
      ', [
        ':date' => $date->format(CalendarDate::GTFS_FORMAT),
      ])->fetchAll(\PDO::FETCH_ASSOC);
      $service_removed = FALSE;
      $service_added = FALSE;
      $route_service_ids = array_map(function ($service) {
        return $service['service_id'];
      }, Route::servicesSource($route_id));
      foreach ($calendarDates as $calendarDate) {
        $exception_type = $calendarDate['exception_type'];
        if ($exception_type === CalendarDate::EXCEPTION_TYPE_ADDED && in_array($calendarDate['service_id'], $route_service_ids)) {
          $service_added = $calendarDate['service_id'];
          $calendar_date = $calendarDate;
        }
        elseif ($exception_type === CalendarDate::EXCEPTION_TYPE_REMOVED) {
          $service_removed = true;
        }
      }
      if ($service_removed && !$service_added) {
        $service_id = FALSE;
      }
      elseif (!!$service_added) {
        $service_id = $service_added;
      }
    }
    $service = Service::getFromSource($service_id);
    return [$service, $calendar_date];
  }

  public function getScheduleFromSource($agency_id, $route_id, $direction_id, $service = null, $calendar_date = null) {
    $meta = $this->initializeMeta();
    $direction = Direction::getFromSource($route_id, $direction_id);
    $agency = Agency::getFromSource($agency_id);
    $route = Route::getFromSource($agency_id, $route_id);
    $meta['direction'] = $direction;
    $meta['route'] = $route;
    $meta['timeFormat'] = $this->currentRequest->query->get('timeFormat');
    if (!$meta['timeFormat']) {
      $meta['timeFormat'] = StopTime::TIME_FORMAT;
    }

    $meta['timeFormat'] = str_replace('%20', ' ', $meta['timeFormat']);

    $url = Drupal::request()->getSchemeAndHttpHost() . str_replace(
      ['{version}', '{agency_id}', '{route_id}'],
      ['source', $agency_id, $route_id],
      self::$url
    );

    $filter = isset($meta['filter']) ? $this->sanitizeFilter($meta['filter'], 'source') : [];

    $data = [
      'stops' => [],
      'trips' => [],
    ];

    if ($calendar_date) {
      $data['calendar_date'] = $calendar_date;
    }

    if ($service) {
      $data['service'] = $service;
    }
    else {
      $data['service'] = 'No service on this date';
      return [$meta, $data];
    }

    $db = Drupal::database();

    // Store feed id for performance
    $frtri = $db->query('
        SELECT MAX(feed_reference__target_revision_id)
        FROM {gtfs_agency_source}
        WHERE `agency_id` = :agency_id
        GROUP BY `agency_id`
    ', [':agency_id' => $agency_id])->fetch(\PDO::FETCH_COLUMN);

    $trips = $db->query('
        SELECT *
        FROM {gtfs_trip_source}
        WHERE `route_id` = :route_id
        AND `direction_id` = :direction_id
        AND `service_id` = :service_id
        AND `feed_reference__target_revision_id` = :frtri
    ', [
      ':route_id' => $route_id,
      ':direction_id' => $direction_id,
      ':service_id' => $service['service_id'],
      ':frtri' => $frtri,
    ])->fetchAll(\PDO::FETCH_ASSOC);

    $stops = [];

    foreach ($trips as $trip) {
      $trip_id = $trip['trip_id'];
      $data['trips'][$trip_id] = $trip;
      $query = '
        SELECT `stop_id`,`stop_sequence`,`arrival_time`
        FROM {gtfs_stop_time_source}
        WHERE `trip_id` = :trip_id
        AND `feed_reference__target_revision_id` = :frtri
      ';
      $placeholders = [
        ':trip_id' => $trip['trip_id'],
        ':frtri' => $frtri,
      ];
      foreach ($filter as $key => $constraints) {
        foreach ($constraints as $operator => $value) {
          $placeholder = ":{$key}";
          $placeholders[$placeholder] = $value;
          if ($operator === 'IN' || $operator === 'NOT IN') {
            $placeholder = "({$placeholder})";
          }
          $query .= " AND `{$key}` {$operator} {$placeholder}";
        }
      }

      $data['trips'][$trip_id]['stop_times'] = $db->query($query, $placeholders)->fetchAll(\PDO::FETCH_ASSOC);

      foreach ($data['trips'][$trip_id]['stop_times'] as $index => &$stop_time) {
        $stop_id = $stop_time['stop_id'];
        $stop_time['stop_sequence'] = (int) $stop_time['stop_sequence'];
        $sequence = $stop_time['stop_sequence'];

        if (!isset($stops[$stop_id])) {
          $stops[$stop_id] = Stop::getFromSource($stop_id);
        }

        if (empty($stops[$stop_id]['stop_sequences'])) {
          $stops[$stop_id]['stop_sequences'] = [];
        }

        if (!in_array($sequence, $stops[$stop_id]['stop_sequences'])) {
          $stops[$stop_id]['stop_sequences'][] = $sequence;
        }
      }
    }

    uasort($stops, function ($a, $b) {
      $a_highest_sequence = max($a['stop_sequences']);
      $b_highest_sequence = max($b['stop_sequences']);
      return $a_highest_sequence - $b_highest_sequence;
    });

    $data['stops'] = array_values($stops);

    foreach ($data['trips'] as &$trip) {
      $trip_stop_times = [];
      foreach ($data['stops'] as $stop) {
        $trip_stops_here = FALSE;
        foreach ($trip['stop_times'] as $trip_stop_time) {
          if (in_array($trip_stop_time['stop_sequence'], $stop['stop_sequences'])) {
            $trip_stops_here = TRUE;
            $trip_stop_times[$trip_stop_time['stop_sequence']] = $trip_stop_time;
          }
        }
        if (!$trip_stops_here) {
          $trip_stop_times[$trip_stop_time['stop_sequence']] = NULL;
        }
      }
      usort($trip_stop_times, function ($a, $b) {
        return $a['stop_sequence'] - $b['stop_sequence'];
      });
      $trip['stop_times'] = array_values($trip_stop_times);
    }

    uasort($data['trips'], [$this, 'tripSorter']);
    uasort($data['trips'], [$this, 'tripSorter']);
    uasort($data['trips'], [$this, 'tripSorter']);

    array_walk($data['trips'], function (&$trip) use ($meta) {
      array_walk($trip['stop_times'], function (&$stop_time) use ($meta) {
        if (!$stop_time) {
          return $stop_time;
        }
        $stop_time['arrival_time'] = StopTime::timeStringToDateTime($stop_time['arrival_time'])->format($meta['timeFormat']);;
      });
    });

    return [$meta, $data];
  }

  /**
   * Given two trips, determines which one to show first.
   *
   * First tries to calculate the earliest stop that the two trips have in common,
   * and if present, sorts on that. If the trips have no common stops, sorts
   * purely on the earliest time.
   * @param $trip1
   * @param $trip2
   *
   * @return float|int
   */
  public static function tripSorter($trip1, $trip2) {
    $trip_1_stops = array_values(array_map(function ($stop_time) {
      return $stop_time['stop_id'];
    }, array_filter($trip1['stop_times'])));

    $trip_2_stops = array_values(array_map(function ($stop_time) {
      return $stop_time['stop_id'];
    }, array_filter($trip2['stop_times'])));

    $intersection = array_intersect($trip_1_stops, $trip_2_stops);

    $trip1_min = 0;
    $trip2_min = 0;

    foreach ($trip1['stop_times'] as $index => $stop_time) {
      if (
        $stop_time
        && $trip1_min === 0
        && (count($intersection) && in_array($stop_time['stop_id'], $trip_2_stops))
      ) {
        $trip1_min = StopTime::parseTime($stop_time['arrival_time']);
      }
    }

    foreach ($trip2['stop_times'] as $index => $stop_time) {
      if (
        $stop_time
        && $trip2_min === 0
        && (count($intersection) && in_array($stop_time['stop_id'], $trip_1_stops))
      ) {
        $trip2_min = StopTime::parseTime($stop_time['arrival_time']);
      }
    }

    return $trip1_min - $trip2_min;


  }

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL) {
    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL) {
    $db = \Drupal::database();

    $meta = $this->initializeMeta();

    if (
      $this->currentRequest->query->get('direction_id') !== NULL
      && (
        $this->currentRequest->query->get('service_id') !== NULL
        || $this->currentRequest->query->get('date') !== NULL
      )
    ) {
      $direction_id = $this->currentRequest->query->get('direction_id');
      [$service, $calendar_date] = $this->getServiceAndCalendarDateFromSource($agency_id, $route_id);
      return $this->getScheduleFromSource($agency_id, $route_id, $direction_id, $service, $calendar_date);
    }


    $data = [];

    $agency = Agency::getFromSource($agency_id);

    $route = Route::getFromSource($agency_id, $route_id);

    $meta['route'] = $route;

    $services = Route::servicesSource($route_id);

    $route_service_ids = array_map(function ($service) {
      return $service['service_id'];
    }, $services);

    $directions = Route::directionsSource($route_id);

    $route_service_ids = array_combine(array_map(function ($service) {
      return $service['service_id'];
    }, $services), $services);

    $base = Drupal::request()->getSchemeAndHttpHost();

    foreach ($services as $service) {
      foreach ($directions as $direction) {
        $url = $base . str_replace(
          ['{version}', '{agency_id}', '{route_id}'],
          ['source', $agency_id, $route_id],
          self::$url
        ) . '?' . http_build_query([
          'direction_id' => $direction['direction_id'],
          'service_id' => $service['service_id'],
        ]);
        $data[] = [
          'direction' => $direction,
          'service' => $service,
          'self' => $url,
        ];
      }
    }

    $upcomingExceptions = $db->query('
      SELECT *
      FROM {gtfs_calendar_date_source}
      WHERE `date` BETWEEN STR_TO_DATE(:now, :format) AND STR_TO_DATE(:then, :format)
      AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_calendar_date_source}
            GROUP BY service_id,date,exception_type
        )
    ', [
      ':now' => (new DateTime('now'))->format('Y-m-d'),
      ':then' => (new DateTime('+2 weeks'))->format('Y-m-d'),
      ':format' => '%Y-%m-%d',
    ])->fetchAll(\PDO::FETCH_ASSOC);

    $accounted_dates = [];


    $upcomingExceptions = array_filter($upcomingExceptions, function ($exception) use (&$accounted_dates) {
      if (in_array($exception['date'], $accounted_dates)) {
        return false;
      } else {
        $accounted_dates[] = $exception['date'];
        return true;
      }
    });

    foreach ($upcomingExceptions as $calendar_date) {
      if (!in_array($calendar_date['service_id'], array_keys($route_service_ids))) continue;
      $service = $route_service_ids[$calendar_date['service_id']];
      foreach ($directions as $direction) {
        $url = $base . str_replace(
            ['{version}', '{agency_id}', '{route_id}'],
            ['source', $agency_id, $route_id],
            self::$url
          ) . '?' . http_build_query([
            'direction_id' => $direction['direction_id'],
            'service_id' => $service['service_id'],
            'date' => $calendar_date['date'],
          ]);
        $data[] = [
          'direction' => $direction,
          'service' => $service,
          'calendar_date' => $calendar_date,
          'self' => $url,
        ];
      }
    }

    return [$meta, $data];
  }

  /**
   * Responds to route GET requests, given an agency and route.
   * If direction and service id or date, returns a schedule for that day.
   * Otherwise returns a calendar of available schedules.
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function v1($agency_id = NULL, $route_id = NULL) {

    $agency = Agency::getById($agency_id);

    $route = Route::getById($agency, $route_id);

    if (
      $this->currentRequest->query->get('direction_id') !== NULL
      && (
        $this->currentRequest->query->get('service_id') !== NULL
        || $this->currentRequest->query->get('date') !== NULL
      )
    ) {
      $direction_id = $this->currentRequest->query->get('direction_id');
      [$service, $calendar_date] = $this->getServiceAndCalendarDate($agency, $route);
      return $this->getSchedule($agency, $route, $direction_id, $service, $calendar_date);
    }

    $db = Drupal::database();

    $data = [];

    $meta = $this->initializeMeta();

    $meta['route'] = $route->toGTFSObject();

    $services = $route->services();

    $directions = array_unique(array_map(function ($trip) {
      return $trip->get('direction_id')->value;
    }, $route->trips()));

    $route_service_ids = array_map(function ($service) {
      return $service->id();
    }, $route->services());

    foreach ($services as $service) {
      if (!in_array($service->id(), $route_service_ids)) continue;
      foreach ($directions as $direction_id) {
        $direction = Direction::getById($route, $direction_id);
        $url = Drupal::request()->getSchemeAndHttpHost() . str_replace(['{agency_id}', '{route_id}'], [$agency_id, $route_id], self::$url) . '?' . http_build_query([
            'direction_id' => $direction_id,
            'service_id' => $service->get('service_id')->value,
          ]);
        Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $service);
        $schedule = [
          'direction' => $direction ? $direction->toGTFSObject() : [
            'direction_id' => $direction_id
          ],
          'service' => $service->toGTFSObject(),
          'self' => $url,
        ];
        $data[] = $schedule;
      }
    }

    $upcomingExceptions = CalendarDate::loadMultiple(Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_calendar_date_field_data}
       WHERE `date` BETWEEN STR_TO_DATE(:now, :format) AND STR_TO_DATE(:then, :format)
       AND `feed_reference__target_id` = :frti",
      [
        ':now' => (new DateTime('now'))->format('Y-m-d'),
        ':then' => (new DateTime('+2 weeks'))->format('Y-m-d'),
        ':format' => '%Y-%m-%d',
        ':frti' => $route->get('feed_reference')->target_id,
      ]
    )->fetchAll(PDO::FETCH_COLUMN, 0));
    
    $accounted_dates = [];

    // Exceptions can be added or removed. We want to account for all of these, but we don't want
    // duplicates if service has been both added and removed (the usual case)
    $upcomingExceptions = array_filter($upcomingExceptions, function ($exception) use (&$accounted_dates) {
      if (in_array($exception->get('date')->value, $accounted_dates)) {
        return false;
      } else {
        $accounted_dates[] = $exception->get('date')->value;
        return true;
      }
    });

    $accounted_dates = [];

    // Exceptions can be added or removed. We want to account for all of these, but we don't want
    // duplicates if service has been both added and removed (the usual case)
    $upcomingExceptions = array_filter($upcomingExceptions, function ($exception) use (&$accounted_dates) {
      if (in_array($exception->get('date')->value, $accounted_dates)) {
        return false;
      } else {
        $accounted_dates[] = $exception->get('date')->value;
        return true;
      }
    });

    foreach ($upcomingExceptions as $calendar_date) {
      if (!in_array($calendar_date->get('service_id')->target_id, $route_service_ids)) continue;
      $service = Service::load($calendar_date->get('service_id')->target_id);
      foreach ($directions as $direction_id) {
        $direction = Direction::getById($route, $direction_id);
        // Preload the GTFS object so it does the proper format calculation.
        $calendar_date_gtfs = $calendar_date->toGTFSObject();
        $schedule = [
          'direction' => $direction ? $direction->toGTFSObject() : [
            'direction_id' => $direction_id
          ],
          'service' => $service->toGTFSObject(),
          'calendar_date' => $calendar_date_gtfs,
          'self' => Drupal::request()->getSchemeAndHttpHost() . str_replace(['{agency_id}', '{route_id}'], [$agency_id, $route_id], self::$url) . '?' . http_build_query([
            'direction_id' => $direction_id,
            'service_id' => $service->get('service_id')->value,
            'date' => $calendar_date_gtfs['date'],
          ]),
        ];
        $data[] = $schedule;
      }
    }

    return [$meta, $data];
  }

  /**
   * Given an agency, route, and direction, returns a schedule.
   * Optionally pass information on a particular service or calendar date.
   */
  public function getSchedule($agency, $route, $direction_id, $service = NULL, $calendar_date = NULL) {

    $meta = $this->initializeMeta();

    $meta['totalCount'] = $meta['dataCount'] = 1;

    $direction = Direction::getById($route, $direction_id);

    $meta['direction'] = $direction ? $direction->toGTFSObject() : [
      'direction_id' => $direction_id
    ];

    $meta['route'] = $route->toGTFSObject();


    $meta['timeFormat'] = $this->currentRequest->query->get('timeFormat');
    if (!$meta['timeFormat']) {
      $meta['timeFormat'] = StopTime::TIME_FORMAT;
    }
    $meta['timeFormat'] = str_replace('%20', ' ', $meta['timeFormat']);

    $url = Drupal::request()->getSchemeAndHttpHost() . str_replace(
      ['{agency_id}', '{route_id}'],
      [$agency->get('agency_id')->value, $route->get('route_id')->value],
      self::$url
    );

    $filter = isset($meta['filter']) ? $this->sanitizeFilter($meta['filter']) : [];

    $data = [
      'stops' => [],
      'trips' => [],
    ];

    if ($calendar_date) {
      $data['calendar_date'] = $calendar_date->toGTFSObject();
    }
    if ($service) {
      $data['service'] = $service->toGTFSObject();
    }
    else {
      $data['service'] = 'No service on this date';
      return [$meta, $data];
    }

    // Alright, let's do the magic.
    $db = Drupal::database();

    // Figure out the entity's ID, not the route ID from the URL.
    // Multiple agencies may share the same route ID, so the entity
    // ID is a way of keeping them straight.
    // Get all the trips pertaining to the route,
    // direction, and service.
    $trips = Drupal::entityTypeManager()->getStorage('gtfs_trip')->loadByProperties([
      'route_id' => $route->id(),
      'direction_id' => $direction_id,
      'service_id' => $service->id(),
    ]);

    // We're going to loop through all the trips,
    // get their stop_times, and then get the associated
    // stops, and put those in order according to the stop times.
    foreach ($trips as $trip) {
      $trip_id = $trip->get('trip_id')->value;

      $data['trips'][$trip_id] = $trip->toGTFSObject();

      // Get all the stop_times associated with the trip.
      $query = "SELECT `id`,`stop_id`,`stop_sequence`,`arrival_time`
       FROM {gtfs_stop_time_field_data}
       WHERE trip_id = :trip_id";

      $placeholders = [':trip_id' => $trip->id()];

      foreach ($filter as $key => $constraints) {
        foreach ($constraints as $operator => $value) {
          $placeholder = ":{$key}";
          $placeholders[$placeholder] = $value;
          if ($operator === 'IN' || $operator === 'NOT IN') {
            $placeholder = "({$placeholder})";
          }
          $query .= " AND `{$key}` {$operator} {$placeholder}";
        }
      }

      // Attach them to the trip for later use.
      $data['trips'][$trip_id]['stop_times'] = $db->query($query, $placeholders)->fetchAll(PDO::FETCH_ASSOC);

      // For each of the trip's stop times.
      foreach ($data['trips'][$trip_id]['stop_times'] as $index => &$stop_time) {
        // Check that stop time's sequence. If it hasn't been entered already,.
        if (
            empty($data['stops'][$stop_time['stop_sequence']])
        ) {

          // Get the associated stop.
          $stop = Stop::load($stop_time['stop_id']);

          // If the stop has already been used with a different sequence, insert
          // it at that one's sequence, and edit the stop_time
          // to match that sequence.
          $index_of_existing = array_search(
            $stop->get('stop_id')->value,
            // https://secure.php.net/manual/en/function.array-column.php#122738
            array_combine(
              array_keys($data['stops']),
              array_column($data['stops'], 'stop_id')
            )
          );

          if ($index_of_existing !== FALSE) {
            $stop_time['stop_sequence'] = $index_of_existing;
          }

          // And then put that into our bucket at the proper index.
          $data['stops'][$stop_time['stop_sequence']] = $stop->toGTFSObject();
        }

        $stop_time['stop_id'] = \Drupal::database()->query(
          'SELECT `stop_id`
          FROM {gtfs_stop_field_data}
          WHERE `id` = :id',
          [':id' => $stop_time['stop_id']]
        )->fetch(\PDO::FETCH_COLUMN);
      }
    }


    // Now sort by key, because they don't come from
    // the database in order.
    ksort($data['stops']);

    // Go through each trip and make a row.
    foreach ($data['trips'] as &$trip) {
      $trip_stop_times = [];
      foreach ($data['stops'] as $stop_sequence => $stop) {
        $trip_stops_here = FALSE;
        foreach ($trip['stop_times'] as $trip_stop_time) {
          if ($trip_stop_time['stop_sequence'] == $stop_sequence) {
            $trip_stops_here = TRUE;
            $trip_stop_times[$stop_sequence] = $trip_stop_time;
          }
        }
        if (!$trip_stops_here) {
          $trip_stop_times[$stop_sequence] = NULL;
        }
      }
      $trip['stop_times'] = array_values($trip_stop_times);
    }

    // Move the sequence from being the key to being a property.
    $data['stops'] = array_map(function ($sequence, $stop) {
      $stop['stop_sequence'] = $sequence;
      return $stop;
    }, array_keys($data['stops']), $data['stops']);

    // Yes, sort it three times. It's because of the offsets and also laziness.
    uasort($data['trips'], [$this, 'tripSorter']);
    uasort($data['trips'], [$this, 'tripSorter']);
    uasort($data['trips'], [$this, 'tripSorter']);

    // Previously we put the raw timestamps in the cells. Now we
    // need to go through and format them.
    array_walk($data['trips'], function (&$trip) use ($meta) {
      array_walk($trip['stop_times'], function (&$stop_time) use ($meta) {
        if (!$stop_time) {
          return $stop_time;
        }
        $stop_time['arrival_time'] = StopTime::timeStringToDateTime($stop_time['arrival_time'])->format($meta['timeFormat']);;
      });
    });

    $data['trips'] = array_values($data['trips']);

    return [$meta, $data];
  }

}
